function tinhTongHaiKySo() {
    var soNguyen = document.getElementById("so").value;
    var a = soNguyen;
    var b = a % 10;
    var c = Math.floor(a/10);
    var sum = parseInt(b) + parseInt(c);
    console.log("Tong của 2 ký số: ", sum);
    document.getElementById("resuft5").innerText = `Số ${soNguyen} có tổng của 2 ký số  ${c} + ${b}  là: ${sum}`
}

// MÔ HÌNH 3 KHỐI 
// INPUT: Một số có 2 chữ số
// PROGRESS:
// -Bước 1: Tạo 1 biến soNguyen tương ướng 2 giá trị số nguyên có 2 chữ só;
// -Bước 2: Tạo biến a tương ứng gán biến a  = soNguyen;
// -Bước 3: Tạo biến b tương ứng gán biến a  = a % 10; ~ để tách hàng đơn vị của số đó ra.
// -Bước 4: Tạo biến c tương ứng gán biến c = Math.floor(a/10); ~ để tách hàng chục chữ số đó ra bằng các chia só a cho 10, và làm tròn xuống.
// -Bước 5: Tạo biến sum  tương ứng gán biến sum  = b + c; ~ để tính tổng 2 ký số
// OUTPUT: Tinh và in giá trị tổng sum của 2 ký số c và b